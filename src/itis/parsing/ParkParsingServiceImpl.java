package itis.parsing;

import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в объект класса "Park", либо бросает исключение с информацией об ошибках обработки
  @Override
    public Park parseParkData(String parkDatafilePath) {
        Class<?> parkClass = Park.class;
        Constructor<?> parkConstructor;
        try {
            parkConstructor = parkClass.getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }

        // Разрешаем доступ к приватному конструктору.
        parkConstructor.setAccessible(true);
        Park park;
        try {
            park = (Park) parkConstructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException(e);
        }
        System.out.println("park = " + park);

        Field[] parkFields = parkClass.getDeclaredFields();

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(parkDatafilePath), UTF_8))) {

            // Считываем данные.
            String line;
            while ((line = br.readLine()) != null) {
                if (!line.equals("***")) {
                    String str = line.replace("\"", "");
                    String[] dataField = str.split(":");
                    String fieldName = dataField[0];
                    String fieldValue = dataField[1];

                    List<ParkParsingException.ParkValidationError> validationErrors = new ArrayList<>();
                    for (Field field : parkFields) {
                        if (field.getName().equals(fieldName)) {
                            if (field.isAnnotationPresent(NotBlank.class) & !(fieldValue.length() > 0)) {
                                validationErrors.add(new ParkParsingException.ParkValidationError(fieldName,
                                        "Поле " + fieldName + " не может быть пустым."));
                            }

                            if (field.isAnnotationPresent(MaxLength.class) & (fieldValue.length() <= field.getAnnotation(MaxLength.class).value())) {
                                validationErrors.add(new ParkParsingException.ParkValidationError(fieldName,
                                        "Поле " + fieldName + " превышает максимальную длину."));
                            }
                        }
                    }

                    if (validationErrors.size() > 0) {
                        throw new ParkParsingException("Ошибка парсинга", validationErrors);
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}